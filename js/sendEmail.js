function sendEmail() {
    if (document.getElementById("form_email").validity.valid
        && document.getElementById("form_name").validity.valid
        && document.getElementById("form_lastname").validity.valid
        && document.getElementById("form_message").validity.valid) {
        var form = document.getElementById("contact-form");
        var url = "https://script.google.com/macros/s/AKfycbwHcfvcyljjg67iM1HVQThQd-S5re9F9h721acK24k3WorCDw4/exec";
        var xhr = new XMLHttpRequest();
        var data = {
            "email" : form.email.value,
            "name" : form.name.value,
            "surname" : form.surname.value,
            "message" : form.message.value
        };
        xhr.open('POST', url);
        // xhr.withCredentials = true;
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function() {
            var thankYouMessage = form.querySelector(".thankMessage");
            if (thankYouMessage) {
                thankYouMessage.style.display = "block";
            }
            form.email.value = "";
            form.name.value = "";
            form.surname.value = "";
            form.message.value = "";
            return false;
        };
        // url encode form data for sending as post data
        var encoded = Object.keys(data).map(function(k) {
            return encodeURIComponent(k) + "=" + encodeURIComponent(data[k]);
        }).join('&');
        xhr.send(encoded);
        // alert("Merci de nous avoir contacté. Nous nous ferons le plaisir de vous répondre très vite.");
    }
}