fetch('https://api.easymeeting.io/customer/logos')
.then(data => {return data.json()})
.then(jsonData => {
    let logos = jsonData.response
    let htmlBlock='';
    for (var i = 0; i < logos.length; i++) {
        htmlBlock = htmlBlock + '<img class="mr-4" width="60" src="'+logos[i].url+'">';
    }
    document.getElementById("partnerWrapper").innerHTML = htmlBlock
})   